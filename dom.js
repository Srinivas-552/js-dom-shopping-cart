
// let userData = {
//     'Mike': [],
//     'John': []
// };

// let itemCount = Object.keys(userData).length;

let itemCount = 0;

//main container
let mainContainer = document.createElement("div");
mainContainer.classList.add("main-container")
document.body.appendChild(mainContainer);

//main heading
let h1 = document.createElement("h1");
h1.textContent = "Shopping Cart";
h1.style.color = 'magneta';
mainContainer.appendChild(h1);

//div for select, input & addButton
let inputContainer = document.createElement("div");
mainContainer.appendChild(inputContainer);

let selectEle = document.createElement("select");
selectEle.classList.add("select-element");
selectEle.setAttribute('id', 'selectedValue');
inputContainer.appendChild(selectEle);

let defaultOption = document.createElement("option");
defaultOption.setAttribute("value", "selectUser");
defaultOption.setAttribute("selected", "selected");
defaultOption.setAttribute("disabled", "disabled");
defaultOption.textContent = "Select User";
selectEle.appendChild(defaultOption);

let optionEle = document.createElement("option");
optionEle.setAttribute("value", "Mike");
optionEle.textContent = "Mike";
selectEle.appendChild(optionEle);

let optionEle2 = document.createElement("option");
optionEle2.setAttribute('value', 'John');
optionEle2.textContent = "John";
selectEle.appendChild(optionEle2);

let inputEle = document.createElement("input");
inputEle.type = "text";
inputEle.classList.add("input");
inputEle.setAttribute('placeholder', 'Add item to cart');
inputContainer.appendChild(inputEle);

let addButton = document.createElement("button");
addButton.textContent = "Add";
addButton.classList.add("add-button");
inputContainer.appendChild(addButton);


//ul container
let ulMike = document.createElement("ul");
ulMike.className = 'mike';
mainContainer.appendChild(ulMike);

let ulJohn = document.createElement("ul");
ulJohn.className = 'john';
mainContainer.appendChild(ulJohn)

let mikeEle = document.querySelector('.mike')
let johnEle = document.querySelector('.john')



function onStatusChange(checkboxId, labelId){
    let checkboxEle = document.getElementById(checkboxId);
    let labelEle = document.getElementById(labelId);
    if(checkboxEle.checked){
        labelEle.classList.add("checked");
    } else {
        labelEle.classList.remove("checked");
    }
}



function createAndAppendItem(user) {
    let checkboxId = "checkbox" + user.uniqueNo;
    let labelId = "label" + user.uniqueNo;
    let itemId = "user" + user.uniqueNo;


    //li container
  let li = document.createElement("li");
  li.classList.add("list-item-container");
  li.id = itemId;
  

  let inputCheckbox = document.createElement("input");
  inputCheckbox.type = "checkbox";
  inputCheckbox.id = checkboxId;
  li.appendChild(inputCheckbox);

  inputCheckbox.onclick = function(){
      onStatusChange(checkboxId, labelId)
  }

  //label container inside ul
  let labelContainer = document.createElement("div");
  labelContainer.classList.add("label-container");
  li.appendChild(labelContainer);

  let labelEle = document.createElement("label");
  labelEle.textContent = user.item;
  labelEle.setAttribute("for", checkboxId);
  labelEle.id = labelId;
  labelEle.style.marginLeft = '10px';
  labelContainer.appendChild(labelEle);


  let deleteButton = document.createElement("button");
  deleteButton.textContent = "Remove";
  deleteButton.classList.add("remove-button");
  labelContainer.appendChild(deleteButton);


  deleteButton.addEventListener('click', () => {
    if (user['name'] === 'Mike'){
        mikeEle.removeChild(li);
    }
    if (user['name'] === 'John'){
        johnEle.removeChild(li);
    }
  })


  if (user['name'] === 'Mike'){
    mikeEle.appendChild(li);
  }
  if (user['name'] === 'John'){
    johnEle.appendChild(li);
  }

}



function addCartItem(userName){
    let userInputVal = inputEle.value;

    if(userInputVal===""){
        alert("Enter a valid cart item");
    } else {
        itemCount = itemCount + 1;
        let newUser  = {
            name: userName, 
            item: userInputVal,
            uniqueNo: itemCount
        }

        createAndAppendItem(newUser)
        inputEle.value = "";
    }
}

addButton.onclick = function(){
    let userName = selectEle.value;
    addCartItem(userName)
}


selectEle.addEventListener('change', (event)=>{
    let selectedUser = event.target.value;
    if(selectedUser==='Mike'){
        mikeEle.style.display = 'block';
        johnEle.style.display = 'none';
    }
    if(selectedUser==='John'){
        mikeEle.style.display = 'none';
        johnEle.style.display = 'block';
    }
});